#scalanlity

It is the measurement of the system abilityto increase or decrease in the performance .

For example: How well a hardware system performs when the number of users is increased,
How well a database withstands growing number of queries,
How well an operating system performs on different classes of hardware.


## Load Balancing Defination:
 It is mainly used to distribute traffic across the server.
 There are lot of problems Load balancing helps to solve like performance,economy,and availility problems.

 ###Load Balancer:
 It is the device which is used to distribute the traffic across the server.
 a load Balanceract as the "traffic cop" sitting in front of the servers and routing client requests 
 across all servers and routing client requests across all servers capable of fulfiling those requests 
 in a manner that maximize speed and capacity utilization. 

####Possiblity of using load balance:-

Horizontal scaling vs vertical scaling:

we will discuss one by one both:

####1.1 Horizontal scaling(Scaling down):
In Horizontal scaling we need to do a load balancing :
For insistance:
if we have a one system then we have  to increased the systems to remove the traffic of the server
then we need loadbalancing to distribute load on every system  equally to handle excessive traffic .

####1.2 Probablity of failure:
For example :- 1 computer = 0.3 
if we increase the server for ex. we have added 10 servers more then probablity of failure get reduced.
10 computers = 0.3 ^ 10 (which is very less in compare to single server)

####1.3: Horizontal scaling needs more more money to increase the amount of servers and need more space.

####2.1 Vertical Scaling:(Scaling up)
In vertical scaling there is no need of load balancing because in it we do not increase the systems to handle 
traffic control , we just maximize the system power.
For better understing:
We have a computer of 1 gb ram and there is lot more traffic which this processor can not handle anymore 
then we increased it 1 to 8 gb now computer can handle more traffic.

####2.2 Probablity of failure :
for 1 computer = 0.3 
it will remain same because the number system is not incresing.
probablity of failure is more in vertical scaling.

####3.3 Vertical scaling needs less amount of money and less space .


#####Possiblity of using load balancers:-
There are three important problem domains that load balancers were made to address: performance, availability, 
and economy.

When faced with mounting demand from users, and maxing out the performance of the machine hosting your service
you have two options: scale up or scale out. Scaling up (i.e., vertical scaling) has physical computational limits. 
Scaling out (i.e., horizontal scaling) allows you to distribute the computational load across as many systems as 
necessary to handle the workload. When scaling out, a load balancer can help distribute the workload among 
array of servers, while also allowing capacity to be added or removed as necessary.

Refrences:-
1.https://youtu.be/3GGPsX-s5-M
2.https://www.oreilly.com/library/view/load-balancing-in/9781492038009/ch01.html
3.https://www.geeksforgeeks.org/overview-of-scaling-vertical-and-horizontal-scaling/

  
